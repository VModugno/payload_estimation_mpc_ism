clear variables 
close all 
clc

%% control flag

generate_func    = false;

start_simulation = true;

visualization    = false;

plotting         = true;

%% MPC model
model_name             = "two_r_robot_0"; %twod_xy_lip_0

%% enviroment and estimator parameters
conf_file_payload    = "robot_payload";
conf_file_no_payload = "robot_no_payload";

cov                  = 100;
noise                = 0.001;
num_pars             = 8;
function_regressor   = "ModelEst.get_2R_Y_licols";
converter_function   = "Robot2RPayloadParameters";

%% MPC generator parameters
% cpp solver to use 
solver       = "QPoases";

%% experiment time structure (for the environment)
%rng(100);
ft         = 50;       % 20 50 
delta_t    = 0.01;    % 0.1 0.01 (to the env class)
t          = 0:delta_t:ft;
%% system -----------------------------------------------------------------
str = "MpcModel." + model_name + ".m";
run(str);
% if the system is already discretized i need to substitute the
% symbolical dt with its actual value
if(discretized)
    A_cont = double(subs(A,delta_t));
    B_cont = double(subs(B,delta_t));
    C_cont = double(subs(C,delta_t));
end
%% enviroment for tracker -------------------------------------------------
reward     = @(x,u)(norm(x));  %(TODO reward class)
env_call   = "Env."+ env_name + "(init_state,delta_t,reward,'ConfigFile','"+ conf_file_payload + "')";
env        = eval(env_call);
%% estimated model-----------------------------------------------------
est_model = ModelEst.EstimatedModel(env,'ConfigFile',conf_file_no_payload);
%% reference---------------------------------------------------------------


x_des    = [q1des_t;q2des_t;dq1des_t;dq2des_t];

%% MPC --------------------------------------------------------------------
% tracker
controller   = MpcGen.genMpcTracker(A_cont,B_cont,C_cont,maxInput,maxOutput,internal_dt,N,state_gain,control_cost,...
                                    type,solver,generate_func,discretized,mutable_constr);
%% estimator---------------------------------------------------------------
est                        = ModelEst.OLS(t,cov,noise,controller.m,num_pars,function_regressor,1);
% converter from coefficients to param
coefficients_to_param_call = "ModelEst."+ converter_function + "('" + conf_file_no_payload + "',est_model.orig_prm)";
update_payload_param       = eval(coefficients_to_param_call);

%update_payload_param = ModelEst.Robot2RPayloadParameters('robot_no_payload',est_model.orig_prm);
                              
%% testing MPC tracking on the environment 

% preparing the reference for testing
total_ref = reshape(x_des,length(x_des)*controller.q,1);
% extend ref by N to avoid the preceeding horizon to exceed the
% final time
last_ref = x_des(:,end);
for i=1:N
    total_ref = [total_ref;last_ref];
end

if(visualization) 
    env.Render();
end
if(start_simulation)
    cur_x           = init_state;
    all_states(:,1) = cur_x;
    for i=1:length(t)-1
        if(visualization)   
            env.UpdateRender(cur_x);  
        end
        
        % model update
        if(i>1)
            est.Update(cur_x,last_action,last_DD,i);
            [param_list,value] = update_payload_param.ComputePayloadParameters(est.GetMean());
            est_model.UpdatePrm(param_list,value);
        end
        
        %% mpc controller  
        tau = controller.ComputeControl(cur_x,total_ref((i-1)*controller.q + 1:(i-1)*controller.q + controller.N*controller.q));
        
        %% feedback linearization when required
        if(feedback_lin)
        %% only for test with MPC tracking with 2r robot for now
            old_tau    = tau;
            dyn_comp   = est_model.ComputeDynamics(cur_x);%env.GetDynamicalComponents(cur_x);
            tau        = dyn_comp.M*tau + dyn_comp.S*cur_x(3:4,1) + dyn_comp.g;
        end
         
        % update env
        [new_state]= env.Step(tau);
        % update variables
        cur_x             = new_state; % + [0.4*randn(2,1) ; 0.4*randn(2,1)];
        all_states(:,i+1) = cur_x;
        all_action(:,i)   = tau;
        last_action       = env.GetMeasuredAction() + 1*randn(2,1);% + noise
        last_DD           = env.GetMeasuredAcc();
    end
end

if(plotting)
    %--------------------------------------------------------------------
    % PLOTS
    %--------------------------------------------------------------------
    %%
    figure
    plot(t,all_states(1,:),t,all_states(2,:),t,q1des_t,t,q2des_t);
    grid;
    xlabel('time');
    ylabel('pos [rad]');
    legend('q_1','q_2','q_1 desired','q_2 desired');

    mL_estimated = update_payload_param.all_payloads(1,:);
    cLx_estimated = update_payload_param.all_payloads(2,:)./mL_estimated;
    cLy_estimated = update_payload_param.all_payloads(3,:)./mL_estimated;
    cLz_estimated = update_payload_param.all_payloads(4,:)./mL_estimated;
    JLzz_estimated = update_payload_param.all_payloads(5,:);

    ConfigFile.payload_2R;

    figure
    t_to_plot = t(1:end-2);
    plot(t_to_plot,mL_estimated,t_to_plot,cLx_estimated,t_to_plot,cLy_estimated,t_to_plot,cLz_estimated,t_to_plot,JLzz_estimated);
    leg_1 = sprintf('mL (real val = %f)',mL);
    leg_2 = sprintf('cLx (real val = %f)',cLx);
    leg_3 = sprintf('cLy (real val = %f)',cLy);
    leg_4 = sprintf('cLz (real val = %f)',cLz);
    leg_5 = sprintf('JLzz (real val = %f)',JLzz);
    legend(leg_1,leg_2,leg_3,leg_4,leg_5);
    grid;
    xlabel('time');
end

%% generating function (do not change this part)
if(generate_func)
    controller.GenFunctions();
    % function to create file with parameters
    controller.GenParametersFile();
    env.GenEnvParametersFile(controller.basepath,ft);
end
