classdef (Abstract) AbstractRobotPayloadParameters < handle
   
    properties 
        prm
       Jpayload
       dynamic_coefficients
       all_payloads
    end
    
    methods
        
        function  obj = AbstractRobotPayloadParameters(robot_conf,nominal_prm)
            obj.prm                  = Utils.CopyPrmFromFile(robot_conf,nominal_prm);
            obj.Jpayload             = obj.JPayloadCurrentRobot();
            obj.dynamic_coefficients = obj.DynamicCoeffCurrentRobot();   
        end
        
        function [param_list,value]=ComputePayloadParameters(obj,XX)
            current_payload_estimation = pinv(obj.Jpayload)*(XX-obj.dynamic_coefficients);
            [param_list,value]         = obj.PayloadParametersCurrentRobot(current_payload_estimation);
            obj.all_payloads           = [obj.all_payloads,current_payload_estimation];
        end
        
       
        
    end
    
    
    
    methods(Abstract)
        Jpayload             = JPayloadCurrentRobot(obj)
        dynamic_coefficients = DynamicCoeffCurrentRobot(obj)
        [param_list,value]   = PayloadParametersCurrentRobot(obj,current_payload_estimation);
    end    
    
        
    
    
end